import { React, render } from 'local/react/example'
import { Root, state } from 'local/react/state'

import { Button } from './index'

const exampleState = state<{ active: boolean }>({ active: false })

render(
  <Root
    render={() => (
      <Button onClick={() => (exampleState.active = !exampleState.active)}>
        Hello world {exampleState.active ? 'active' : ''}
      </Button>
    )}
  />
)
