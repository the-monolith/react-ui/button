import { component, React } from 'local/react/component'

export const Button = component.children
  .props<{ onClick: () => void }>()
  .render(({ children, ...props }) => <button {...props}>{children}</button>)
